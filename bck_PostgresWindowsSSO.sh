#!/bin/bash
token=$(oc whoami -t)
log=./Log_bck_mongo.txt
validateToken=$(oc login https://consoleapi.claro.co:443 --token=$token 2>&1)


executeTask(){
	validateToken=$validateToken
	echo "Ejecutando backup..$(date)" >>$log
	echo "Obteniendo y validando token"  >>$log
	echo "Ingresando al proyecto transversales-security" >>$log
	oc project transversales-security  >>$log
	echo "Obteniendo pod de sso-postgresql"  >>$log
	podPostgres=$(oc get pods | grep sso-postgresql | grep Running |awk '{print $1}')  >>$log
	#podPostgres2=$(oc get pods | grep db-seguridad-validador-identidad | grep Running | awk '{print $1}')  >>$log
	echo "Creando port-forward" >>$log
	oc port-forward ${podPostgres} 27018:5432 &
	sleep 2 && pg_dump -U admin -h localhost -p 27018 -d sso73pgsql >./bck_sso-postgresql_$(date +%d%m%Y).sql
	#oc port-forward ${podPostgres2} 27019:5432 &
	#sleep 2 && pg_dump -U inh_validador -h localhost -p 27019 -d inh_validador > ~/Desktop/bck_db-seguridad_$(date +%d%m%Y).sql
	echo "comprimiendo backup" >> $log
	tar -czvf bck_sso-postgresql_$(date +%d%m%Y).tar.gz bck_sso-postgresql_$(date +%d%m%Y).sql 
	#bck_db-seguridad_$(date +%d%m%Y).sql >>$log
	echo "Borrando archivo" >>$log
	rm -r bck_sso-postgresql_$(date +%d%m%Y).sql >>$log
	#rm -r bck_db-seguridad_$(date +%d%m%Y).sql >>$log
	echo "Backup finalizado!" >>$log
	echo "Matando procesos" >> $log
	procesos=$(tasklist | grep oc.exe | awk '{print $2}')
	pros=$(echo $procesos | tr " " "\n")
	for proceso in $pros
	do
		taskkill //PID $proceso //F
	done
	echo "Procesos eliminados"
	exit
}

if [[ $validateToken -ne "error: The token provided is invalid or expired." ||  $validateToken -ne "error: no token is currently in use for this session" || $validateToken -ne "You must obtain an API token by visiting https://consoleapi.claro.co:443/oauth/token/request" ]]
then        
	executeTask		
else
	echo "Token expirado"  >>$log
	oc login -u 'admin' -p 'Fo&i1ypRLleR'
	executeTask
fi		

if [[ $validateToken -ne "error: The token provided is invalid or expired." ||  $validateToken -ne "error: no token is currently in use for this session" || $validateToken -ne "You must obtain an API token by visiting https://consoleapi.claro.co:443/oauth/token/request" ]]
then
	executeTask	
else
	echo "Token expirado"  >>$log
	oc login -u 'admin' -p 'Fo&i1ypRLleR'
	executeTask
fi



